package ru.frosteye.ovsa.data.container;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.frosteye.ovsa.marker.Concatenable;
import ru.frosteye.ovsa.marker.UniqueItem;

/**
 * Created by user on 12.03.16.
 */
public class NonSerializableEntityContainer<T extends UniqueItem & Comparable<T>> implements Concatenable {
    //TODO make it thread safe
    private List<T> items;

    public NonSerializableEntityContainer() {
        this(new ArrayList<T>());
    }

    public NonSerializableEntityContainer(@NonNull List<T> items) {
        this.items = items;
    }

    public int size() {
        return items.size();
    }

    public void add(T t) {
        items.add(t);
    }

    public void add(int pos, T t) {
        items.add(pos, t);
    }

    public void remove(T t) {
        items.remove(t);
    }

    public void remove(int pos) {
        items.remove(pos);
    }

    public void sort() {
        Collections.sort(items);
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public T findById(long id) {
        for(T t: items) {
            if(t.getId() == id) return t;
        }
        return null;
    }

    public List<T> getItems() {
        return items;
    }

    public T get(int pos) {
        return items.get(pos);
    }

    @Override
    public String toIdsString() {
        if(items.isEmpty()) return null;
        StringBuilder builder = new StringBuilder();
        for(T item: items) {
            builder.append(",").append(item.getId());
        }
        return builder.toString().substring(1, builder.length());
    }

    public List<T> filter(CharSequence charSequence) {
        List<T> results = new ArrayList<>();
        if(charSequence == null || charSequence.length() == 0) {
            results.addAll(getItems());
            return results;
        }
        for(T entity: getItems()) {
            try {
                if(applyTextFilter(entity, charSequence))
                    results.add(entity);
            } catch (Exception ignored) {}
        }
        return results;
    }

    public List<T> customFilter(Object object) {
        ArrayList<T> results = new ArrayList<>();
        if(object == null) {
            results.addAll(getItems());
            return results;
        }
        for(T entity: getItems()) {
            try {
                if(applyCustomFilter(entity, object))
                    results.add(entity);
            } catch (Exception ignored) {}
        }
        return results;
    }

    protected boolean applyCustomFilter(T entity, Object predicate) {
        return true;
    }

    protected boolean applyTextFilter(T entity, CharSequence charSequence) {
        return true;
    };
}
