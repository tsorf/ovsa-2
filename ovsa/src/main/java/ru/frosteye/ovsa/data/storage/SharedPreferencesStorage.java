package ru.frosteye.ovsa.data.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.serialization.Serializer;


public class SharedPreferencesStorage implements Storage {
    
    private static final Set<Class<?>> PRIMITIVES;
    
    static {
        PRIMITIVES = new HashSet<>();
        PRIMITIVES.add(Boolean.class);
        PRIMITIVES.add(Character.class);
        PRIMITIVES.add(Byte.class);
        PRIMITIVES.add(Short.class);
        PRIMITIVES.add(Integer.class);
        PRIMITIVES.add(Long.class);
        PRIMITIVES.add(Float.class);
        PRIMITIVES.add(Double.class);
        PRIMITIVES.add(Void.class);
    }

    private static boolean isPrimitive(Object object) {
        if(object == null) return false;
        return PRIMITIVES.contains(object.getClass());
    }

    private SharedPreferences sharedPreferences;
    private Serializer serializer;

    @Inject
    public SharedPreferencesStorage(SharedPreferences sharedPreferences, Serializer serializer) {
        this.sharedPreferences = sharedPreferences;
        this.serializer = serializer;
    }

    @Override
    public void save(String key, Object object) {
        if(isPrimitive(object)) {
            object = object.toString();
        }
        sharedPreferences.edit().putString(key, serializer.serialize(object)).commit();
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        return serializer.deserialize(sharedPreferences.getString(key, null), clazz);
    }

    @Override
    public Serializer serializer() {
        return serializer;
    }
}
