package ru.frosteye.ovsa.data.storage;

/**
 * Created by ovcst on 06.04.2017.
 */

public interface Repo<T> {
    T load();
    void save(T t);
    void executeAndSave(RepoRunnable<T> runnable);

    interface RepoRunnable<T> {
        void run(T entity);
    }
}
