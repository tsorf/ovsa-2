package ru.frosteye.ovsa.data.storage;

/**
 * Created by ovcst on 08.06.2017.
 */

public abstract class BaseRepo<T> implements Repo<T> {

    protected Storage storage;
    protected T model;

    public BaseRepo(Storage storage) {
        this.storage = storage;
    }

    @Override
    public synchronized T load() {
        if(model == null) {
            model = storage.get(provideClass().getName(), provideClass());
        }
        if(model == null) {
            model = handleNull();
        }
        return model;
    }

    @Override
    public synchronized void executeAndSave(RepoRunnable<T> runnable) {
        T object = load();
        runnable.run(object);
        save(object);
    }

    protected T handleNull() {
        return null;
    }

    protected abstract Class<T> provideClass();

    @Override
    public synchronized void save(T t) {
        this.model = t;
        storage.save(provideClass().getName(), model);
    }
}
