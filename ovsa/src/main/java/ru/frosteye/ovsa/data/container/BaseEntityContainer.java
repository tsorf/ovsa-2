package ru.frosteye.ovsa.data.container;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleg on 20.11.2017.
 */

public class BaseEntityContainer<Item> {

    private List<Item> items = new ArrayList<>();

    public BaseEntityContainer() {
    }

    public BaseEntityContainer(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }
}
