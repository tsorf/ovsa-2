package ru.frosteye.ovsa.data.storage;


import ru.frosteye.ovsa.execution.serialization.Serializer;

public interface Storage {
    void save(String key, Object object);
    <T> T get(String key, Class<T> clazz);
    Serializer serializer();
}
