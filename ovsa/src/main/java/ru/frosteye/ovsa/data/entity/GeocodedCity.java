package ru.frosteye.ovsa.data.entity;

/**
 * Created by oleg on 15.12.2017.
 */

public class GeocodedCity {
    private String name;
    private double lat, lng;

    public GeocodedCity(String name, double lat, double lng) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    @Override
    public String toString() {
        return name;
    }
}
