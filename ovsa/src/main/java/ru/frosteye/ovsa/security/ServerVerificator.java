package ru.frosteye.ovsa.security;

import android.content.Context;
import android.provider.Settings;

import com.google.gson.Gson;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by oleg on 04.07.16.
 */
public class ServerVerificator implements Verificator {
    public static final String URL = "http://verificator.frosteye.ru/";
    private Callback callback;
    private Context appContext;
    private Api api;
    private String androidId;

    @Inject
    public ServerVerificator(Context appContext) {
        this.appContext = appContext.getApplicationContext();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(Api.class);
        androidId = Settings.Secure.getString(appContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void performCheck(final Callback callback) {
        this.callback = callback;
        api.check(appContext.getPackageName(), androidId).enqueue(new retrofit2.Callback<VerificationResponse>() {
            @Override
            public void onResponse(Call<VerificationResponse> call,
                                   Response<VerificationResponse> response) {
                callback.onResult(response.body());
            }

            @Override
            public void onFailure(Call<VerificationResponse> call, Throwable t) {
                callback.onResult(new VerificationResponse(t.getMessage(), 0));
            }
        });
    }

    public static class VerificationResponse implements Result {
        private String message;
        private int code;

        public VerificationResponse(String message, int code) {
            this.message = message;
            this.code = code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public boolean isSuccessful() {
            return code == 200;
        }
    }

    public interface Api {
        @POST("check")
        @FormUrlEncoded
        Call<VerificationResponse> check(@Field("package") String pack,
                                         @Field("androidId") String androidId);
    }
}
