package ru.frosteye.ovsa.security;

/**
 * Created by oleg on 04.07.16.
 */
public interface Verificator {
    int CODE_FORBIDDEN = 403;
    int CODE_OK = 200;
    int CODE_NOT_FOUND = 404;

    void performCheck(Callback callback);

    interface Callback {
        void onResult(Result result);
    }

    interface Result {
        String getMessage();
        int getCode();
        boolean isSuccessful();
    }
}
