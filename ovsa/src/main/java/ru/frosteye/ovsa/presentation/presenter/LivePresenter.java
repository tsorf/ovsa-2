package ru.frosteye.ovsa.presentation.presenter;

/**
 * Created by oleg on 15.06.16.
 */

import android.content.Intent;

import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity;

/**
 * {@link Presenter} with some lifecycle (should be bound to {@link PresenterActivity}'s lifecycle)
 * @param <T> {@link Presenter}'s view type
 */
public interface LivePresenter<T> extends Presenter<T> {
    void onResume();
    void onPause();
    void onStop();
    void onDestroy();
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
