package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleableRes;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by oleg on 23.12.16.
 */

public abstract class BaseTextView extends AppCompatTextView {
    public BaseTextView(Context context) {
        super(context);
    }

    public BaseTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource()));
    }

    public BaseTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource(), defStyleAttr, 0));
    }

    @StyleableRes
    protected int[] getStyleableResource() {
        return null;
    }

    private void init(TypedArray array) {
        onTypedArrayReady(array);
        array.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        prepareView();
    }

    protected void onTypedArrayReady(TypedArray array) { }

    protected abstract void prepareView();
}
