package ru.frosteye.ovsa.presentation.view.widget;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by oleg on 16.01.2018.
 */

public class HeartBeatImageView extends BaseImageView {

    public static final int DEFAULT_DURATION = 700;
    public static final int BOOST_DURATION = 200;

    private ObjectAnimator objectAnimator;

    public HeartBeatImageView(Context context) {
        super(context);
    }

    public HeartBeatImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeartBeatImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void prepareView() {
        objectAnimator = ObjectAnimator.ofPropertyValuesHolder(this,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        objectAnimator.setDuration(DEFAULT_DURATION);

        objectAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        objectAnimator.setRepeatMode(ObjectAnimator.REVERSE);
    }

    public void startBeating() {
        objectAnimator.start();
    }

    public void stopBeating() {
        objectAnimator.cancel();
    }

    public void boost() {
        objectAnimator.setDuration(BOOST_DURATION);
    }

    public void calmDown() {
        objectAnimator.setDuration(DEFAULT_DURATION);
    }
}
