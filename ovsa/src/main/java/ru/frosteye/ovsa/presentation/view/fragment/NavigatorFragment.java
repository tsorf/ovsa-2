package ru.frosteye.ovsa.presentation.view.fragment;

import ru.frosteye.ovsa.presentation.navigation.ActionBarItemDelegate;
import ru.frosteye.ovsa.presentation.navigation.Navigator;
import ru.frosteye.ovsa.presentation.navigation.NavigatorView;

/**
 * Created by ovcst on 03.08.2017.
 */

public abstract class NavigatorFragment<T extends NavigatorView> extends PresenterFragment implements ActionBarItemDelegate {

    private Navigator<T> navigator;

    public void setNavigator(Navigator<T> navigator) {
        this.navigator = navigator;
    }

    public Navigator<T> getNavigator() {
        return navigator;
    }

    @Override
    public void onBarAction(int id) {

    }
}
