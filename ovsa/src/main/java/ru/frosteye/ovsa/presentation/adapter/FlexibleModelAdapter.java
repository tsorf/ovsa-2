package ru.frosteye.ovsa.presentation.adapter;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.ovsa.presentation.view.InteractiveModelView;

/**
 * Created by oleg on 17.08.17.
 */

public class FlexibleModelAdapter<T extends IFlexible> extends FlexibleAdapter<T> {

    private InteractiveModelView.Listener listener;

    public FlexibleModelAdapter(@Nullable List<T> items) {
        super(items);
    }

    public FlexibleModelAdapter(@Nullable List<T> items, @Nullable Object listeners) {
        super(items, listeners);
    }

    public FlexibleModelAdapter(@Nullable List<T> items, @Nullable Object listeners,
                                boolean stableIds) {
        super(items, listeners, stableIds);
    }

    public FlexibleModelAdapter(@Nullable List<T> items, InteractiveModelView.Listener listener) {
        super(items);
        this.listener = listener;
    }

    public FlexibleModelAdapter(@Nullable List<T> items, @Nullable Object listeners,
                                InteractiveModelView.Listener listener) {
        super(items, listeners);
        this.listener = listener;
    }

    public FlexibleModelAdapter(InteractiveModelView.Listener listener) {
        super(new ArrayList<T>());
        this.listener = listener;
    }

    public FlexibleModelAdapter(@Nullable List<T> items, @Nullable Object listeners,
                                boolean stableIds, InteractiveModelView.Listener listener) {
        super(items, listeners, stableIds);
        this.listener = listener;
    }

    public InteractiveModelView.Listener getListener() {
        return listener;
    }
}
