package ru.frosteye.ovsa.presentation.effect;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.frosteye.ovsa.R;

/**
 * Created by oleg on 21.12.2017.
 */

public class GridDivider extends RecyclerView.ItemDecoration {

    private Context context;
    private int span = 1;
    private int spacing;

    public GridDivider(Context context, int span) {
        this.context = context;
        if(span < 1) {
            throw new IllegalStateException("span < 1!");
        }
        this.span = span;
        this.spacing = context.getResources().getDimensionPixelSize(R.dimen.default_grid_spacing);
    }

    public GridDivider(Context context, int span, @DimenRes int spacing) {
        this(context, span);
        this.spacing = context.getResources().getDimensionPixelSize(spacing);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int column = position % span;
        boolean lastRow = position >= parent.getAdapter().getItemCount() / span;
        if(column == 0) {
            outRect.set(spacing, spacing, spacing, lastRow ? spacing : 0);
        } else {
            outRect.set(0, spacing, spacing, lastRow ? spacing : 0);
        }
    }
}
