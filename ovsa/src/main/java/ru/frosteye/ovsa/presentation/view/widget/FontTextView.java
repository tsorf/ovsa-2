package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.CallSuper;
import android.support.annotation.StyleableRes;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by oleg on 23.12.16.
 */

public abstract class FontTextView extends BaseTextView {

    private static Typeface typeface;

    public FontTextView(Context context) {
        super(context);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override @CallSuper
    protected void prepareView() {
        if(typeface == null) {
            typeface = Typeface.createFromAsset(getContext().getAssets(), customTypeFace());
        }
        setTypeface(typeface);
    }

    protected abstract String customTypeFace();
}
