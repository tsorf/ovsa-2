package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.StyleableRes;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by oleg on 23.12.16.
 */

public abstract class BaseImageView extends AppCompatImageView {
    public BaseImageView(Context context) {
        super(context);
    }

    public BaseImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource()));
    }

    public BaseImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource(), defStyleAttr, 0));
    }

    @StyleableRes
    protected int[] getStyleableResource() {
        return null;
    }

    private void init(TypedArray array) {
        onTypedArrayReady(array);
        array.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        prepareView();
    }

    protected void onTypedArrayReady(TypedArray array) { }

    protected abstract void prepareView();
}
