package ru.frosteye.ovsa.presentation.view;

/**
 * Created by oleg on 25.09.17.
 */

public interface IPrompt {
    String title();
    String hint();
    String positiveButton();
    String negativeButton();
    int inputType();

    interface Listener {
        void onResult(String text);
        void onCancel();
    }
}
