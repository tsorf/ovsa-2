package ru.frosteye.ovsa.presentation.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import ru.frosteye.ovsa.presentation.view.InteractiveModelView;
import ru.frosteye.ovsa.presentation.view.ModelView;

/**
 * Created by oleg on 12.07.17.
 */

public abstract class AdapterItem<T, V extends View & ModelView<T>>
        extends AbstractFlexibleItem<ModelViewHolder<V>>  {

    private T model;

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public AdapterItem(T model) {
        this.model = model;
    }

    public AdapterItem() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdapterItem<?, ?> that = (AdapterItem<?, ?>) o;

        return model != null ? model.equals(that.model) : that.model == null;

    }

    @Override
    public abstract int getLayoutRes();

    @Override
    public int hashCode() {
        return model != null ? model.hashCode() : 0;
    }

    @Override
    public ModelViewHolder<V> createViewHolder(View view, FlexibleAdapter adapter) {
        ModelViewHolder<V> holder = new ModelViewHolder<>(view, adapter);
        if(adapter instanceof FlexibleModelAdapter) {
            holder.setListener(((FlexibleModelAdapter) adapter).getListener());
        }
        return holder;
    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, ModelViewHolder<V> holder, int position,
                               List payloads) {

        holder.view.setModel(model);
    }
}
