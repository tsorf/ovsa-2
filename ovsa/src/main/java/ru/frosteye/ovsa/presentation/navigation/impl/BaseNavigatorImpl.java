package ru.frosteye.ovsa.presentation.navigation.impl;

import android.view.MenuItem;

import ru.frosteye.ovsa.presentation.navigation.ActionBarItemDelegate;
import ru.frosteye.ovsa.presentation.navigation.Navigator;
import ru.frosteye.ovsa.presentation.navigation.NavigatorView;

/**
 * Created by oleg on 10.07.17.
 */

public abstract class BaseNavigatorImpl<V extends NavigatorView> implements Navigator<V> {

    private V view;
    private ActionBarItemDelegate actionBarItemDelegate;

    @Override
    public void onAttachView(V view) {
        this.view = view;
    }

    public V getView() {
        return view;
    }

    public ActionBarItemDelegate getActionBarItemDelegate() {
        return actionBarItemDelegate;
    }

    public void setActionBarItemDelegate(
            ActionBarItemDelegate actionBarItemDelegate) {
        this.actionBarItemDelegate = actionBarItemDelegate;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(getActionBarItemDelegate() != null) {
            getActionBarItemDelegate().onBarAction(item.getItemId());
        }

        return true;
    }
}
