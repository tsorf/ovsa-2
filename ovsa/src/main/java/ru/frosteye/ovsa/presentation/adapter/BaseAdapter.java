package ru.frosteye.ovsa.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleg on 01.07.16.
 */

/**
 * Use FlexibleAdapter instead
 * @param <T>
 * @param <V>
 */
@Deprecated
public abstract class BaseAdapter<T, V extends BaseAdapter.ViewHolder> extends RecyclerView.Adapter<V> {
    private List<T> items = new ArrayList<>();
    protected LayoutInflater inflater;
    private Context context;
    private ClickCallback<T> clickCallback;
    private LongClickCallback<T> longClickCallback;

    public BaseAdapter(Context context, ClickCallback<T> clickCallback) {
        this.inflater = LayoutInflater.from(context);
        this.clickCallback = clickCallback;
        this.context = context;
    }

    public BaseAdapter(Context context, LongClickCallback<T> longClickCallback) {
        this.inflater = LayoutInflater.from(context);
        this.longClickCallback = longClickCallback;
        this.context = context;
    }

    public BaseAdapter(Context context, ClickCallback<T> clickCallback, LongClickCallback<T> longClickCallback) {
        this.inflater = LayoutInflater.from(context);
        this.longClickCallback = longClickCallback;
        this.clickCallback = clickCallback;
        this.context = context;
    }

    public BaseAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public List<T> getItems() {
        return items;
    }

    public void updateItems(List<T> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        this.items.clear();
        notifyDataSetChanged();
    }

    public void updateItems(List<T> items, boolean append) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public ViewHolder(View itemView) {
            super(itemView);
            if(clickCallback != null)
                itemView.setOnClickListener(this);
            if(longClickCallback != null)
                itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickCallback.onAdapterItemSelected(getItem(getAdapterPosition()));
        }

        @Override
        public boolean onLongClick(View view) {
            longClickCallback.onAdapterItemSelected(getItem(getAdapterPosition()));
            return true;
        }
    }

    public interface ClickCallback<T> {
        void onAdapterItemSelected(T item);
    }

    public interface LongClickCallback<T> {
        void onAdapterItemSelected(T item);
    }
}
