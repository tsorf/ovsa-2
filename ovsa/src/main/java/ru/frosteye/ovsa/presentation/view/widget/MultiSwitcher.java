package ru.frosteye.ovsa.presentation.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.frosteye.ovsa.R;

/**
 * Created by oleg on 27.06.16.
 */
public class MultiSwitcher extends LinearLayout implements View.OnClickListener {
    public interface Callback {
        void onSwitcherItemSelected(int position, CharSequence text);
    }

    public static final String DIVIDER_PERPENDICULAR = "|";
    public static final String DIVIDER_DIAGONAL = "/";
    private LayoutInflater layoutInflater;
    private CharSequence[] items;
    private String selectedDivider = DIVIDER_DIAGONAL;
    private Callback callback;
    private int activeIndex = 0;


    public MultiSwitcher(Context context) {
        super(context);
    }

    public MultiSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.MultiSwitcher));
    }

    public MultiSwitcher(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.MultiSwitcher, defStyleAttr, 0));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MultiSwitcher(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context.obtainStyledAttributes(attrs, R.styleable.MultiSwitcher, defStyleAttr, defStyleRes));
    }

    private void init(TypedArray array) {
        if(array.hasValue(R.styleable.MultiSwitcher_items)) {
            this.items = array.getTextArray(R.styleable.MultiSwitcher_items);
        }
        if(array.hasValue(R.styleable.MultiSwitcher_itemsDivider)) {
            this.selectedDivider = array.getString(R.styleable.MultiSwitcher_itemsDivider);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.layoutInflater = LayoutInflater.from(getContext());
        processItems();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setItems(CharSequence[] items, Callback callback) {
        this.callback = callback;
        this.items = items;
        processItems();
    }

    public void setSelected(int index) {
        this.activeIndex = index;
        processItems();
    }

    public void setSelected(CharSequence value) {
        for(int i = 0; i < items.length; i++) {
            if(value.equals(items[i])) {
                activeIndex = i;
                processItems();
                return;
            }
        }
    }

    public CharSequence[] getItems() {
        return items;
    }

    private void processItems() {
        this.removeAllViews();
        if(items == null || items.length == 0) return;
        for(int i = 0; i < items.length; i++) {
            TextView view = createTextView(activeIndex == i);
            TextView divider = createDividerTextView();
            view.setText(items[i]);
            divider.setText(selectedDivider);
            view.setOnClickListener(this);
            addView(view);
            addView(divider);
        }
        removeViewAt(getChildCount() - 1);

    }

    @Override
    public void onClick(View view) {
        if(callback == null) return;
        int index = indexOfChild(view);
        if((index + 1) % 2 == 0) return;
        activeIndex = index / 2;
        callback.onSwitcherItemSelected(activeIndex, items[activeIndex]);
        processItems();
    }

    private TextView createTextView(boolean selected) {
        return (TextView) layoutInflater.inflate(selected ?
                R.layout.ovsa_multi_switcher_item_selected : R.layout.ovsa_multi_switcher_item, this, false);
    }

    private TextView createDividerTextView() {
        return (TextView) layoutInflater.inflate(R.layout.ovsa_multi_switcher_item_divider, this, false);
    }
}
