package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by ovcst on 20.03.2017.
 */

public class SquareRecyclerView extends RecyclerView {
    public SquareRecyclerView(Context context) {
        super(context);
    }

    public SquareRecyclerView(Context context,
                              @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, widthSpec);
    }
}
