package ru.frosteye.ovsa.presentation.adapter;

import android.view.View;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.FlexibleViewHolder;
import ru.frosteye.ovsa.presentation.view.InteractiveModelView;

/**
 * Created by oleg on 12.07.17.
 */

public class ModelViewHolder<V extends View> extends FlexibleViewHolder {

    public final V view;
    private InteractiveModelView.Listener listener;

    public ModelViewHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);
        this.view = ((V) view);
    }

    public void setListener(InteractiveModelView.Listener listener) {
        this.listener = listener;
        if(listener != null && (view instanceof InteractiveModelView)) {
            ((InteractiveModelView) view).setListener(listener);
        }
    }
}
