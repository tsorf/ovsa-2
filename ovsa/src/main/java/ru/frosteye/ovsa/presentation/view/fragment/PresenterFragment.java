package ru.frosteye.ovsa.presentation.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import ru.frosteye.ovsa.R;
import ru.frosteye.ovsa.presentation.effect.sweetdialog.SweetAlertDialog;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

/**
 * Created by oleg on 15.06.16.
 */
public abstract class PresenterFragment extends OvsaFragment  {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        prepareView(view);
        initView(view, savedInstanceState);
        initView(view);
        attachPresenter();
    }

    protected void prepareView(View view) {}

    /**
     * @deprecated use initView(View view, Bundle savedInstanceState)
     * @param view
     */
    protected void initView(View view) {

    };

    protected void initView(View view, Bundle savedInstanceState) {

    };

    protected abstract void attachPresenter();
    protected abstract LivePresenter<?> getPresenter();

    @Override
    public void onPause() {
        super.onPause();
        if(getPresenter() != null)
            getPresenter().onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getPresenter() != null)
            getPresenter().onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(getPresenter() != null)
            getPresenter().onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getPresenter() != null)
            getPresenter().onDestroy();
    }


}
