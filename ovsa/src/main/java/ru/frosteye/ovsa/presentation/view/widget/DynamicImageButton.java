package ru.frosteye.ovsa.presentation.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.OnClick;
import ru.frosteye.ovsa.R;
import ru.frosteye.ovsa.tool.picture.ImageLoader;
import ru.frosteye.ovsa.tool.picture.ImageTarget;

/**
 * Created by user on 02.07.16.
 */
public class DynamicImageButton extends RelativeLayout implements ImageTarget {
    private CharSequence text;
    private RoundImageView image;
    private TextView button;

    public DynamicImageButton(Context context) {
        super(context);
    }

    public DynamicImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.DynamicImageButton));
    }

    public DynamicImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.DynamicImageButton, defStyleAttr, 0));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DynamicImageButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context.obtainStyledAttributes(attrs, R.styleable.DynamicImageButton, defStyleAttr, defStyleRes));
    }

    private void init(TypedArray array) {
        if(array.hasValue(R.styleable.DynamicImageButton_imageText)) {
            text = array.getString(R.styleable.DynamicImageButton_imageText);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inflate(getContext(), R.layout.ovsa_dynamic_imageview, this);
        image = (RoundImageView)findViewById(R.id.ovsa_dynamicImageView_image);
        button = (TextView)findViewById(R.id.ovsa_dynamicImageView_button);
        button.setText(text);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        image.setOnClickListener(l);
    }

    public CharSequence getText() {
        return text;
    }

    public void setText(CharSequence text) {
        this.text = text;
    }

    @Override
    public ImageView getView() {
        return image;
    }
}
