package ru.frosteye.ovsa.presentation.view;

/**
 * Created by oleg on 17.08.17.
 */

public interface InteractiveModelView<T> extends ModelView<T> {
    void setListener(Listener listener);

    interface Listener {
        void onModelAction(int code, Object payload);
    }
}
