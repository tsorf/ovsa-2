package ru.frosteye.ovsa.presentation.view;

import java.util.List;

/**
 * Created by oleg on 01.07.16.
 */
public interface BaseListView<T> extends BaseView {
    void showItems(List<T> items);
}
