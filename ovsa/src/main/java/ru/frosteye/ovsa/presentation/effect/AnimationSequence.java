package ru.frosteye.ovsa.presentation.effect;

import android.view.View;
import android.view.animation.Animation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import ru.frosteye.ovsa.stub.impl.SimpleAnimationListener;

/**
 * Created by oleg on 17.08.16.
 */
public class AnimationSequence {
    private Deque<Animation> animations = new ArrayDeque<>();
    private List<Animation> backUp = new ArrayList<>();
    private Listener listener;
    private boolean repeat = false;

    public void addAnimation(Animation animation) {
        this.animations.add(animation);
        this.backUp.add(animation);
    }

    public void start(View view, Listener listener) {
        this.listener = listener;
        reset();
        run(view, animations.poll());
    }

    private void run(final View view, Animation animation) {
        if(animation == null) {
            listener.onComplete();
            if(repeat)
                start(view, listener);
            return;
        }
        animation.setAnimationListener(new SimpleAnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                run(view, animations.poll());
            }
        });
        view.startAnimation(animation);
    }

    public void reset() {
        animations.clear();
        for(Animation animation: backUp) {
            animation.reset();
            animations.add(animation);
        }
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public interface Listener {
        void onComplete();
    }
}
