package ru.frosteye.ovsa.presentation.view;

/**
 * Created by user on 03.07.16.
 */
public interface ModelView<T> {
    void setModel(T model);
    T getModel();
}
