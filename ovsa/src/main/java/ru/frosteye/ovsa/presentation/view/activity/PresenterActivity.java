package ru.frosteye.ovsa.presentation.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.ovsa.presentation.presenter.Presenter;
import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.ovsa.tool.UITools;

/**
 * Created by oleg on 15.06.16.
 */
public abstract class PresenterActivity extends OvsaActivity implements BasicView {

    private Bundle savedInstanceState;
    private boolean enabled;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        prepareView();
        initView();
        initView(savedInstanceState);
        attachPresenter();

    }

    @Override @CallSuper
    public void enableControls(boolean enabled, int code) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getPresenter() != null)
            getPresenter().onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(getPresenter() != null)
            getPresenter().onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(getPresenter() != null)
            getPresenter().onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(getPresenter() != null)
            getPresenter().onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(getPresenter() != null)
            getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Override to perform any view preparations - ButterKnife.bind() etc.
     */
    protected void prepareView() {}

    /**
     * This activity's view initialization point.
     * @deprecated use initView(Bundle savedInstanceState)
     */
    @Deprecated
    protected void initView() {

    };

    protected void initView(Bundle savedInstanceState) {

    };

    /**
     * Should contain activity's presenter initialization logic. Be sure to call {@link Presenter#onAttach(Object)} here.
     */
    protected abstract void attachPresenter();

    /**
     * Activity's presenter getter.
     * @return The {@link LivePresenter} instance, associated with this activity.
     */
    protected abstract LivePresenter<?> getPresenter();
}
