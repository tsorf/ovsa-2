package ru.frosteye.ovsa.presentation.presenter;

/**
 * Created by oleg on 15.06.16.
 */
public interface Presenter<T> {
    void onAttach(T t);
}
