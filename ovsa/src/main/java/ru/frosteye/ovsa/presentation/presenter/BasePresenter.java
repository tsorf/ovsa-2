package ru.frosteye.ovsa.presentation.presenter;

import android.content.Intent;
import android.support.annotation.CallSuper;

import ru.frosteye.ovsa.presentation.view.BasicView;

/**
 * Created by ovcst on 08.06.2017.
 */

public abstract class BasePresenter<V extends BasicView> implements LivePresenter<V> {

    protected V view;


    @Override @CallSuper
    public void onAttach(V v) {
        this.view = v;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    protected void showMessage(CharSequence message) {
        view.showMessage(message, 0);
    }

    protected void showError(CharSequence message) {
        view.showMessage(message, -1);
    }

    protected void showSuccess(CharSequence message) {
        view.showMessage(message, 1);
    }

    protected void enableControls(boolean enabled) {
        view.enableControls(enabled, 0);
    }
}
