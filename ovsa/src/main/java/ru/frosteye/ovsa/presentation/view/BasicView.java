package ru.frosteye.ovsa.presentation.view;

/**
 * Created by ovcst on 06.04.2017.
 */

public interface BasicView {

    int CONTROLS_CODE_ERROR = -1;
    int CONTROLS_CODE_SUCCESS = 1;
    int CONTROLS_CODE_PROGRESS = 0;

    void enableControls(boolean enabled, int code);
    void showMessage(CharSequence message, int code);

}
