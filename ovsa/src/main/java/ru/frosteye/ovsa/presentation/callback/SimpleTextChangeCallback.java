package ru.frosteye.ovsa.presentation.callback;

import android.text.Editable;

/**
 * Created by oleg on 27.06.16.
 */
public interface SimpleTextChangeCallback {
    void textChanged(Editable s);
}