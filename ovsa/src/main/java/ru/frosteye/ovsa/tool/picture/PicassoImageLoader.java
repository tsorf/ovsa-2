package ru.frosteye.ovsa.tool.picture;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Inject;

/**
 * Created by user on 02.07.16.
 */
public class PicassoImageLoader implements ImageLoader {
    private Context context;

    @Inject
    public PicassoImageLoader(Context context) {
        this.context = context;
    }

    @Override
    public void load(File file, ImageTarget target) {
        Picasso.with(context).load(file).fit().into(target.getView());
    }

    @Override
    public void load(int res, ImageTarget target) {
        Picasso.with(context).load(res).fit().into(target.getView());
    }

    @Override
    public void load(String url, ImageTarget target) {
        Picasso.with(context).load(url).fit().into(target.getView());
    }
}
