package ru.frosteye.ovsa.tool;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.frosteye.ovsa.R;

/**
 * Created by oleg on 01.07.16.
 */
public class DateTools {

    public interface DatePickerCallback {
        void onDateSelected(Date date);
    }

    public interface RangePickerCallback {
        void onDateSelected(Date startDate, Date endDate);
    }

    public interface TimePickerCallback {
        void onTimeSelected(String time);
        void onRaw(int hour, int minute);
    }

    private static SimpleDateFormat dottedDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private static SimpleDateFormat dashedDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat slashedDateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    private static SimpleDateFormat dottedDateFormatWithTime = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
    private static SimpleDateFormat dottedDateFormatWithTimeAndSeconds = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private static SimpleDateFormat timeFormatMinutesSeconds = new SimpleDateFormat("mm:ss", Locale.getDefault());
    private static SimpleDateFormat dayOfMonthFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
    private static SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());

    public static String formatDottedDateWithTime(Date date) {
        if(date == null) return null;
        return dottedDateFormatWithTime.format(date);
    }

    public static String formatDottedDateWithTimeWithSeconds(Date date) {
        if(date == null) return null;
        return dottedDateFormatWithTimeAndSeconds.format(date);
    }

    public static String formatTime(Date date) {
        if(date == null) return null;
        return timeFormat.format(date);
    }

    public static String formatTimeCounter(Date date) {
        if(date == null) return null;
        return timeFormatMinutesSeconds.format(date);
    }

    public static String formatDashedDate(Date date) {
        if(date == null) return null;
        return dashedDateFormat.format(date);
    }

    public static String formatPrettyDate(Date date) {
        if(date == null) return null;
        return prettyDateFormat.format(date);
    }

    public static String formatDottedDate(Date date) {
        if(date == null) return null;
        return dottedDateFormat.format(date);
    }

    public static String formatDayOfMonth(Date date) {
        if(date == null) return null;
        return dayOfMonthFormat.format(date);
    }

    public static Date parseSlashedDate(String date) {
        try {
            return slashedDateFormat.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    private static final Calendar birthCalendar = Calendar.getInstance();

    //FIXME говно
    public static int calculateAge(Date date) {
        int age;
        synchronized (birthCalendar) {
            birthCalendar.setTime(date);
            Calendar today = Calendar.getInstance();
            age = today.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);
            if (today.get(Calendar.DAY_OF_YEAR) < birthCalendar.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }
        }
        return age;
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback) {
        showDateDialog(context, callback, null, null);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Date minDate) {
        showDateDialog(context, callback, null, minDate);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Calendar initial) {
        showDateDialog(context, callback, initial, null);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Calendar initial, Date minDate) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        if(minDate != null) {
            datePicker.setMinDate(minDate.getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
            }
        });
        d.show();
    }

    public static void showDateDialog(Context context, Date minDate, Date maxDate, final DatePickerCallback callback) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(minDate != null) {
            datePicker.setMinDate(minDate.getTime());
        }
        if(maxDate != null) {
            datePicker.setMaxDate(maxDate.getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
            }
        });
        d.show();
    }

    public static void showDateDialogRange(Activity context, final RangePickerCallback callback, Calendar initial) {
        SmoothDateRangePickerFragment smoothDateRangePickerFragment = SmoothDateRangePickerFragment.newInstance(
                new SmoothDateRangePickerFragment.OnDateRangeSetListener() {
                    @Override
                    public void onDateRangeSet(SmoothDateRangePickerFragment view,
                                               int yearStart, int monthStart,
                                               int dayStart, int yearEnd,
                                               int monthEnd, int dayEnd) {
                        Calendar startDate = Calendar.getInstance();
                        startDate.set(Calendar.YEAR, yearStart);
                        startDate.set(Calendar.MONTH, monthStart);
                        startDate.set(Calendar.DAY_OF_MONTH, dayStart);
                        Calendar endDate = Calendar.getInstance();
                        endDate.set(Calendar.YEAR, yearEnd);
                        endDate.set(Calendar.MONTH, monthEnd);
                        endDate.set(Calendar.DAY_OF_MONTH, dayEnd);
                        callback.onDateSelected(startDate.getTime(), endDate.getTime());
                        view.dismiss();
                    }
                });

        smoothDateRangePickerFragment.show(context.getFragmentManager(), "smoothDateRangePicker");
    }

    public static void showDateDialogWithButtons(Context context, final DatePickerCallback callback, Calendar initial) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static void showDateDialogWithButtons(Context context, final DatePickerCallback callback, Calendar initial, boolean fromToday) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        if(fromToday) {
            datePicker.setMinDate(new Date().getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static void showDateDialogWithButtonsMature(Context context, final DatePickerCallback callback) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        Calendar initial = Calendar.getInstance();
        initial.add(Calendar.YEAR, - 18);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        datePicker.setMaxDate(initial.getTime().getTime());
        initial.add(Calendar.YEAR, - 80);
        datePicker.setMinDate(initial.getTime().getDate());
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static String formatTime(int hour, int minute) {
        String h = hour < 10 ? "0" + hour : String.valueOf(hour);
        String m = minute < 10 ? "0" + minute : String.valueOf(minute);
        return String.format("%s:%s", h, m);
    }

    public static void showTimeDialogWithButtons(Context context, final TimePickerCallback callback) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.time_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hour, min;
                if (Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = timePicker.getHour();
                    min = timePicker.getMinute();
                } else {
                    hour = timePicker.getCurrentHour();
                    min = timePicker.getCurrentMinute();
                }
                callback.onRaw(hour, min);
                callback.onTimeSelected(formatTime(hour, min));
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static void showTimeDialog(Context context, final TimePickerCallback callback) {
        Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.time_picker, null);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int hour, min;
                if (Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = timePicker.getHour();
                    min = timePicker.getMinute();
                } else {
                    hour = timePicker.getCurrentHour();
                    min = timePicker.getCurrentMinute();
                }
                callback.onRaw(hour, min);
                callback.onTimeSelected(formatTime(hour, min));
            }
        });
        d.show();
    }

    public static int countDaysBetween(Date d1, Date d2) {
        int daysdiff = 0;
        long diff = d2.getTime() - d1.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    public static String formatMillisecondsToTime(int time) {
        int minutes = time / (60 * 1000);
        int seconds = (time / 1000) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }

    public static String formatSecondsToTime(int time) {
        int minutes = time / (60);
        int seconds = (time) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }
}
