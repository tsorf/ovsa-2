package ru.frosteye.ovsa.tool.picture;

import android.widget.ImageView;

import java.io.File;

/**
 * Created by user on 02.07.16.
 */
public interface ImageLoader {
    void load(File file, ImageTarget target);
    void load(int res, ImageTarget target);
    void load(String url, ImageTarget target);
}
