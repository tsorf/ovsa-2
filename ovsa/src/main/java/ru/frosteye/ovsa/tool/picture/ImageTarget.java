package ru.frosteye.ovsa.tool.picture;

import android.widget.ImageView;

/**
 * Created by user on 02.07.16.
 */
public interface ImageTarget {
    ImageView getView();
}
