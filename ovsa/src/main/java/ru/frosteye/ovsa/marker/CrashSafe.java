package ru.frosteye.ovsa.marker;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by oleg on 22.12.16.
 */

@Retention(RetentionPolicy.SOURCE)
public @interface CrashSafe {
}
