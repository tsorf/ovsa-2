package ru.frosteye.ovsa.execution.network.client;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by oleg on 21.06.17.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface Authentification {
}
