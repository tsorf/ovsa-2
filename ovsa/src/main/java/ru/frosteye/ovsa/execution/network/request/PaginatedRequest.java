package ru.frosteye.ovsa.execution.network.request;

/**
 * Created by oleg on 02.12.2017.
 */

public class PaginatedRequest {
    private int page = 1;
    private int initialLimit = 15;
    private int limit = initialLimit;

    public int getLimit() {
        return limit;
    }

    public PaginatedRequest setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public int getPage() {
        return page;
    }

    public PaginatedRequest setPage(int page) {
        this.page = page;
        return this;
    }

    public void reset() {
        page = 1;
        limit = initialLimit;
    }

    public void increase() {
        page++;
    }
}
