package ru.frosteye.ovsa.execution.network.response;

import java.util.List;

/**
 * Created by ovcst on 07.01.2017.
 */

public class ArrayResponse<T> {

    private List<T> items;
    private Meta meta;

    public Meta getMeta() {
        return meta;
    }

    public List<T> getItems() {
        return items;
    }

    public static class Meta {
        private int total;
        private int page;

        public int getTotal() {
            return total;
        }

        public int getPage() {
            return page;
        }
    }
}
