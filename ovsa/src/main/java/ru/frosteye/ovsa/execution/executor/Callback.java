package ru.frosteye.ovsa.execution.executor;

/**
 * Created by oleg on 22.09.17.
 */

public interface Callback<T> {
    void onResult(T result);
}
