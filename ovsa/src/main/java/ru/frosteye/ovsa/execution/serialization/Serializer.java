package ru.frosteye.ovsa.execution.serialization;

/**
 * Created by user on 25.06.16.
 */
public interface Serializer {
    /**
     * Perform serialization mechanism on source {@link Object}
     * @param object {@link Object} to serialize
     * @return {@link String}, representing source object;
     */
    String serialize(Object object);

    /**
     * Perform deserialization mechanism on source {@link String} instance.
     * @param string {@link String} to deserialize
     * @param typeOfT resulting object's type
     * @param <T> any type implementation able to deserialize
     * @return deserialized object
     */
    <T> T deserialize(String string, Class<T> typeOfT);
}
