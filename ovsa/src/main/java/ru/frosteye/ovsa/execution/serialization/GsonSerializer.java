package ru.frosteye.ovsa.execution.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;

/**
 * Created by user on 25.06.16.
 */
public class GsonSerializer implements Serializer  {
    private Gson gson;

    @Inject
    public GsonSerializer() {
        this(new GsonBuilder()
        .create());
    }

    public GsonSerializer(Gson gson) {
        this.gson = gson;
    }

    @Override
    public String serialize(Object object) {
        return gson.toJson(object);
    }

    @Override
    public <T> T deserialize(String string, Class<T> typeOfT) {
        return gson.fromJson(string, typeOfT);
    }
}
