package ru.frosteye.ovsa.execution.task;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.client.FixedKladrClient;

/**
 * Created by oleg on 21.09.17.
 */

public class LoadCitiesTask extends ObservableTask<String, List<String>> {

    private FixedKladrClient fixedKladrClient;
    private int limit = 3;

    @Inject
    public LoadCitiesTask(MainThread mainThread,
                          Executor executor, FixedKladrClient fixedKladrClient) {
        super(mainThread, executor);
        this.fixedKladrClient = fixedKladrClient;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    protected Observable<List<String>> prepareObservable(final String string) {
        return Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<List<String>> subscriber) throws Exception {
                try {
                    List<String> cities = fixedKladrClient.getCityNames(string, limit);
                    subscriber.onNext(cities);
                    subscriber.onComplete();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }
}
