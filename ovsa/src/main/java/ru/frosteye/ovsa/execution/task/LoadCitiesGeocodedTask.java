package ru.frosteye.ovsa.execution.task;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.client.FixedKladrClient;
import ru.frosteye.ovsa.tool.Geolocator;

/**
 * Created by oleg on 21.09.17.
 */

public class LoadCitiesGeocodedTask extends ObservableTask<String, List<String>> {

    private FixedKladrClient fixedKladrClient;
    private int limit = 3;
    private Geolocator geolocator;

    @Inject
    public LoadCitiesGeocodedTask(MainThread mainThread,
                                  Executor executor, FixedKladrClient fixedKladrClient,
                                  Geolocator geolocator) {
        super(mainThread, executor);
        this.fixedKladrClient = fixedKladrClient;
        this.geolocator = geolocator;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    protected Observable<List<String>> prepareObservable(final String string) {
        return Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<List<String>> subscriber) throws Exception {
                try {
                    List<String> cities = fixedKladrClient.getCityNames(string, limit);
                    for(String city: cities) {

                    }
                    subscriber.onNext(cities);
                    subscriber.onComplete();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }
}
