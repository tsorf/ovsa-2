package ru.frosteye.ovsa.execution.task;


import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

/**
 * Created by user on 16.06.16.
 */
public class SimpleSubscriber<T> implements Subscriber<T> {

    public static final int ERROR = 0;
    public static final int SUCCESS = 1;

    @Override
    public void onComplete() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onSubscribe(Subscription s) {

    }

    @Override
    public void onNext(T t) {

    }
}
