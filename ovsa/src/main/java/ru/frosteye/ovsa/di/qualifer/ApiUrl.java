package ru.frosteye.ovsa.di.qualifer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by oleg on 22.09.17.
 */

@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface ApiUrl {
}
