package ru.frosteye.ovsa.di.qualifer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by oleg on 16.08.16.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface AndroidId {
}
